from flask import request
from flask import jsonify
from flask import Flask

app = Flask(__name__)

@app.route('/hello',methods=['GET','POST'])
def hello():
    message=request.get_json(force=True)
    name = message['name']
    response = {
        'Greeting': 'Hello, '+name+'!'
    }
    return jsonify(response)
