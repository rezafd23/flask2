# show images inline
# import inline as inline
# import matplotlib as matplotlib
# %matplotlib inline

# automatically reload modules when they have changed
# %load_ext autoreload
# %autoreload 2
import h5py
import numpy as np
np.__version__
# import keras
import keras
# import miscellaneous modules
import matplotlib.pyplot as plt
import cv2
cv2.__version__
import os
import sys
import numpy as np
import time
import tensorflow as tf


from flask import Flask, render_template, request, redirect, url_for, send_from_directory, jsonify
from werkzeug import secure_filename
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color
from keras.layers import MaxPooling2D,ZeroPadding2D,Conv2D,BatchNormalization
from keras.layers import Activation,initializers,Add
# import numpy as np
# import six.moves.urllib as urllib

# from collections import defaultdict
from io import StringIO
from PIL import Image
sys.path.append("..")
# from utils import label_map_util
# from utils import visualization_utils as vis_util

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = 'static/uploads/'
app.config['UPLOAD_SIMULASI'] = 'static/simulasi/'
app.config['RESULT_FOLDER'] = 'results/'
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'jpg', 'jpeg'])

hasil_z=''
hasil_mp=''
hasil_conv1=''
hasil_conv3=''
hasil_conv12=''
hasil_sc=''
hasil_ak=''
hasil_bn=''
hasil_rl=''


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/simulasi',methods=['GET','POST'])
def simulasi():
    return render_template('generic.html')

@app.route('/Example',methods=['GET','POST'])
def UploadExample():
    file = request.files['file']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_SIMULASI'], filename))
        return redirect(url_for('ZeroPaddingExample', filename=filename))

@app.route('/Example/ZeroPad/<filename>')
def ZeroPaddingExample(filename):
    PATH_TO_TEST_IMAGES_DIR = app.config['UPLOAD_SIMULASI']
    TEST_IMAGE_PATHS = [os.path.join(PATH_TO_TEST_IMAGES_DIR, filename.format(i)) for i in range(1, 2)]
    global hasil_z
    global hasil_mp
    global hasil_conv1
    global hasil_conv3
    global hasil_conv12
    global hasil_sc
    global hasil_ak
    global hasil_bn
    global hasil_rl

    for img_path in TEST_IMAGE_PATHS:
        image = cv2.imread(img_path)
        image = cv2.resize(image,(8,8))

        image = image[:, :, ::-1].copy()

        image = image.astype(np.float32)
        image = np.expand_dims(image, axis=0)
        img=image.tolist()
        image = tf.convert_to_tensor(image)

        X = ZeroPadding2D((3, 3))(image)
        hasil_ZeroPadding2D = tf.Variable(X)

        # Convolusi 2d 7x7
        X = Conv2D(64, (7, 7), strides=(2, 2), name='conv1',
                   kernel_initializer=initializers.glorot_uniform(seed=0))(X)
        hasil_Conv2D = tf.Variable(X)

        # BatchNorm
        X = BatchNormalization(axis=3, name='bn_conv1')(X)
        hasil_BatchNorm = tf.Variable(X)

        # ReLu Activation
        X = Activation('relu')(X)
        hasil_ActReLu = tf.Variable(X)

        # MaxPooling 3x3
        X = MaxPooling2D((3, 3), strides=(2, 2))(X)
        hasil_MaxPool = tf.Variable(X)
        Z = X
        # First Residual
        # Conv1x1 64, 2s
        X = Conv2D(64, (1, 1), strides=(2, 2), padding='valid',
                   kernel_initializer=initializers.glorot_uniform(seed=0))(X)
        hasil_conv1x1 = tf.Variable(X)
        X = BatchNormalization(axis=3)(X)
        X = Activation('relu')(X)

        # Conv3x3 64, 2s
        X = Conv2D(64, (1, 1), strides=(1, 1), padding='same',
                   kernel_initializer=initializers.glorot_uniform(seed=0))(X)
        hasil_conv3x3 = tf.Variable(X)
        X = BatchNormalization(axis=3)(X)
        X = Activation('relu')(X)

        # Conv1x1 256, 2s
        X = Conv2D(256, (1, 1), strides=(1, 1), padding='valid',
                   kernel_initializer=initializers.glorot_uniform(seed=0))(X)
        hasil_conv1x1_2 = tf.Variable(X)
        X = BatchNormalization(axis=3)(X)
        X = Activation('relu')(X)

        # Conv1x1 256, 2s
        Z = Conv2D(256, (1, 1), strides=(2, 2), padding='valid',
                   kernel_initializer=initializers.glorot_uniform(seed=0))(Z)
        hasil_shortcut = tf.Variable(Z)
        Z = BatchNormalization(axis=3)(Z)

        # Merge
        X = Add()([X, Z])
        X = Activation('relu')(X)
        hasil_akhir = tf.Variable(X)

        sess = tf.InteractiveSession()
        sess.run(tf.initialize_all_variables())
        print("Result")
        hasil_ZeroPadding2D = sess.run(hasil_ZeroPadding2D)
        hasil_Conv2D = sess.run(hasil_Conv2D)
        hasil_Conv2D= hasil_Conv2D.tolist()
        hasil_z=hasil_Conv2D
        hasil_BatchNorm = sess.run(hasil_BatchNorm)
        hasil_BatchNorm = hasil_BatchNorm.tolist()
        hasil_bn=hasil_BatchNorm
        hasil_ActReLu = sess.run(hasil_ActReLu)
        hasil_ActReLu = hasil_ActReLu.tolist()
        hasil_rl=hasil_ActReLu
        print(hasil_Conv2D)
        print(hasil_BatchNorm)
        print(hasil_ActReLu)
        hasil_MaxPool = sess.run(hasil_MaxPool)
        hasil_MaxPool = hasil_MaxPool.tolist()
        hasil_mp = hasil_MaxPool
        hasil_conv1x1 = sess.run(hasil_conv1x1)
        hasil_conv1x1 = hasil_conv1x1.tolist()
        hasil_conv1 = hasil_conv1x1
        hasil_conv3x3 = sess.run(hasil_conv3x3)
        hasil_conv3x3 = hasil_conv3x3.tolist()
        hasil_conv3=hasil_conv3x3
        hasil_conv1x1_2 = sess.run(hasil_conv1x1_2)
        hasil_conv1x1_2 = hasil_conv1x1_2.tolist()
        hasil_conv12=hasil_conv1x1_2
        hasil_shortcut = sess.run(hasil_shortcut)
        hasil_shortcut = hasil_shortcut.tolist()
        hasil_sc=hasil_shortcut
        hasil_akhir = sess.run(hasil_akhir)
        hasil_akhir = hasil_akhir.tolist()
        hasil_ak=hasil_akhir


        # hasil_ZeroPadding2D = tf.Variable(image)
        # sess = tf.InteractiveSession()
        # sess.run(tf.initialize_all_variables())
        # print("Result")
        # hasil_ZeroPadding2D = sess.run(hasil_ZeroPadding2D)
        hasil_ZeroPadding2D=hasil_ZeroPadding2D.tolist()
        # print(hasil_ZeroPadding2D)

        return jsonify(hasil_ZeroPadding2D=hasil_ZeroPadding2D,img=img,path=filename)

@app.route('/Example/Conv2D/', methods=['GET','POST'])
def Conv2DExample():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_Conv2D=hasil_z)

@app.route('/Example/BatchNorm/', methods=['GET','POST'])
def BatchNormExample():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_BatchNorm=hasil_bn)

@app.route('/Example/ReLu/', methods=['GET','POST'])
def ReLuExample():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_ReLu=hasil_rl)

@app.route('/Example/MaxPool/', methods=['GET','POST'])
def MaxPoolExample():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_MaxPool=hasil_mp)

@app.route('/Example/Conv1/', methods=['GET','POST'])
def Conv1Example():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_Conv1=hasil_conv1)

@app.route('/Example/Conv3/', methods=['GET','POST'])
def Conv3Example():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_Conv3=hasil_conv3)

@app.route('/Example/Conv12/', methods=['GET','POST'])
def Conv12Example():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_Conv12=hasil_conv12)

@app.route('/Example/Shortcut/', methods=['GET','POST'])
def ShortcutExample():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_sc=hasil_sc)

@app.route('/Example/Akhir/', methods=['GET','POST'])
def AkhirExample():
    # global hasil_z
    # hasil_conv2=hasil_z.tolist()
    return jsonify(hasil_ak=hasil_ak)

@app.route('/upload', methods=['GET','POST'])
def upload():
    file = request.files['file']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return redirect(url_for('uploaded_file',filename=filename))
        # return render_template('index.html')

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    PATH_TO_TEST_IMAGES_DIR = app.config['UPLOAD_FOLDER']
    TEST_IMAGE_PATHS = [os.path.join(PATH_TO_TEST_IMAGES_DIR, filename.format(i)) for i in range(1, 2)]

    pred_50=filename

    for image_path in TEST_IMAGE_PATHS:
        # keras.backend.tensorflow_backend.set_session(get_session())
        #
        # model_path = os.path.join('snapshotsbaru', sorted(os.listdir('snapshotsbaru'), reverse=True)[0])
        #
        # # load retinanet model
        # model = models.load_model(model_path, backbone_name='resnet101')
        # model = models.convert_model(model)
        # print(model.summary())
        #
        labels_to_names = {0: 'Car', 1: 'Terinfeksi'}
        image = read_image_bgr(image_path)
        image2 = image

        # copy to draw on
        draw = image.copy()
        draw2 = image.copy()
        draw3 = image.copy()
        draw4 = image.copy()
        draw5 = image.copy()
        draw6 = image.copy()
        unpreprocess=filename
        # draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

        fileoriginal=app.config['UPLOAD_FOLDER']+filename
        filename.partition('.')
        filename, separator, extension = filename.partition('.')
        fileprepocess='static/preprocessing/'+filename+"-preprocess.jpg"
        filenameFPN=filename+"-FPN.jpg"
        filenameFPN2=filename+"-FPN(Unpreprocessing).jpg"


        # preprocess image for network
        preproc=preprocess_image(image)
        image = preprocess_image(image)
        image, scale = resize_image(image)
        # image2, scale2 = resize_image(image2)

        #write Image for Preprocessing
        cv2.imwrite(fileprepocess,preproc)

        # # process image
        # start = time.time()
        # boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))
        # print("processing time: ", time.time() - start)

        boxes,scores,labels=predictdata(image)
        # boxes2,scores2,labels2=predictdata50(image)

        # correct for image scale
        boxes /= scale
        # boxes2 /= scale2

        filedrawbox=drawUntreshold(boxes,scores,labels,draw2,filenameFPN)
        # filedrawbox2=drawUntreshold(boxes2,scores2,labels2,draw4,filenameFPN2)
        filedrawtrbox=drawtreshold(boxes,scores,labels,draw3,filenameFPN)
        # filedrawtrbox2=drawtreshold(boxes2,scores2,labels2,draw5,filenameFPN2)

        Car = 0
        # Normal2 = 0
        # Terinfeksi = 0
        # Terinfeksi2 = 0
        # visualize detections
        for box, score, label in zip(boxes[0], scores[0], labels[0]):
            # scores are sorted so we can break    draw_box(draw, b, color=color)

            if score < 0.5:
                break

            color = label_color(label)

            b = box.astype(int)
            draw_box(draw, b, color=color, thickness=2)

            caption = "{} {:.3f}".format(labels_to_names[label], score)
            draw_caption(draw, b, caption)
            print(caption)

            if label == 0:
                Car = Car + 1
            else:
                Terinfeksi = Terinfeksi + 1

        # Index = round(((Terinfeksi / (Normal + Terinfeksi)) * 100),2)
        # IndexVal=str(Index)+"%"
        print("Mobil= ", Car)
        # print("Eritrosit Terinfeksi= ", Terinfeksi)
        # print("Index Parasit= ",IndexVal)

        # if Index > 0:
        #     diagnosis = "Terkena Malaria"
        #     print("Diagnosis Pasien = "+diagnosis)
        # else:
        #     diagnosis = "Tidak Terkena Malaria"
        #     print("Diagnosis Pasien = "+diagnosis)

        # filename.partition('.')
        # filename,separator,extension=filename.partition('.')
        file=filename+'-test.jpg'
        plt.figure(figsize=(20, 20))
        plt.axis('off')
        # plt.imshow(draw)
        cv2.imwrite('static/results/'+file,draw)
        # draw=plt.gcf()
        # draw.savefig('static/results/'+file,format='jpg')
        directoryfile="/static/results/"+file
        # plt.show()

        # for box2, score2, label2 in zip(boxes2[0], scores2[0], labels2[0]):
        #     # scores are sorted so we can break    draw_box(draw, b, color=color)
        #
        #     if score2 < 0.5:
        #         break
        #
        #     color = label_color(label2)
        #
        #     b2 = box2.astype(int)
        #     draw_box(draw6, b2, color=color, thickness=4)
        #
        #     caption = "{} {:.3f}".format(labels_to_names[label2], score2)
        #     draw_caption(draw6, b2, caption)
        #     print(caption)
        #
        #     if label2 == 0:
        #         Normal2 = Normal2 + 1
        #     else:
        #         Terinfeksi2 = Terinfeksi2 + 1

        # Index2 = round(((Terinfeksi2 / (Normal2 + Terinfeksi2)) * 100), 2)
        # IndexVal2 = str(Index2) + "%"
        # print("Eritrosit Normal= ", Normal2)
        # print("Eritrosit Terinfeksi= ", Terinfeksi2)
        # print("Index Parasit= ", IndexVal2)

        # if Index2 > 0:
        #     diagnosis2 = "Terkena Malaria"
        #     print("Diagnosis Pasien = " + diagnosis2)
        # else:
        #     diagnosis2 = "Tidak Terkena Malaria"
        #     print("Diagnosis Pasien = " + diagnosis2)

        # filename.partition('.')
        # filename,separator,extension=filename.partition('.')
        file2 = filename + '-test2.jpg'
        plt.figure(figsize=(20, 20))
        plt.axis('off')
        # plt.imshow(draw)
        cv2.imwrite('static/results/' + file2, draw6)
        # draw=plt.gcf()
        # draw.savefig('static/results/'+file,format='jpg')
        directoryfile2 = "/static/results/" + file2

        # directoryfile_50, directoryfile2_50, fileprepocess_50,filedrawbox_50, filedrawbox2_50, filedrawtrbox_50,\
        # filedrawtrbox2_50,Normal_50,Terinfeksi_50, Normal2_50, Terinfeksi2_50, IndexVal_50, diagnosis_50, IndexVal2_50, \
        # diagnosis2_50=predictdata50(pred_50)
        # return render_template('index.html')
        return jsonify(Name=directoryfile,Name2=directoryfile2, OriginalFile=fileoriginal, Preprocessing=fileprepocess,
                       Drawbox=filedrawbox, Treshold=filedrawtrbox, EritNormal=Car)
                       # IndexVal=IndexVal, Diagnosis=diagnosis,IndexVal2=IndexVal2, Diagnosis2=diagnosis2)
                       # Name_50=directoryfile_50, Name2_50=directoryfile2_50, Preprocessing_50=fileprepocess_50,
                       # Drawbox_50=filedrawbox_50, Drawbox2_50=filedrawbox2_50, Treshold_50=filedrawtrbox_50,
                       # Treshold2_50=filedrawtrbox2_50,EritNormal_50=Normal_50, EritInfek_50=Terinfeksi_50,
                       # EritNormal2_50=Normal2_50, EritInfek2_50=Terinfeksi2_50,IndexVal_50=IndexVal_50,
                       # Diagnosis_50=diagnosis_50, IndexVal2_50=IndexVal2_50, Diagnosis2_50=diagnosis2_50)

    # return send_from_directory(app.config['RESULT_FOLDER'],filename)
def get_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)

def predictdata(ImageFile):
    keras.backend.tensorflow_backend.set_session(get_session())

    model_path = os.path.join('dataset101', sorted(os.listdir('dataset101'), reverse=True)[0])
    # model_path = os.path.join('snapshotsbaru', sorted(os.listdir('snapshotsbaru'), reverse=True)[0])

    # load retinanet model
    model = models.load_model(model_path, backbone_name='resnet101')
    model = models.convert_model(model)
    print(model.summary())

    # labels_to_names = {0: 'Normal', 1: 'Terinfeksi'}

    # process image
    start = time.time()
    boxes, scores, labels = model.predict_on_batch(np.expand_dims(ImageFile, axis=0))
    print("processing time: ", time.time() - start)
    return boxes,scores,labels

def predictdata50(ImageFile):
    keras.backend.tensorflow_backend.set_session(get_session())

    model_path = os.path.join('dataset50', sorted(os.listdir('dataset50'), reverse=True)[0])

    # load retinanet model
    model = models.load_model(model_path, backbone_name='resnet101')
    model = models.convert_model(model)
    print(model.summary())

    # labels_to_names = {0: 'Normal', 1: 'Terinfeksi'}

    # process image
    start = time.time()
    boxes, scores, labels = model.predict_on_batch(np.expand_dims(ImageFile, axis=0))
    print("processing time: ", time.time() - start)
    return boxes,scores,labels

def drawUntreshold(boxes,scores,labels,draw,file):
    # visualize detections
    for box, score, label in zip(boxes[0], scores[0], labels[0]):
        # scores are sorted so we can break    draw_box(draw, b, color=color)

        color = label_color(label)

        b = box.astype(int)
        draw_box(draw, b, color=color, thickness=4)

    filename='static/FPN/'+file+'-FPN.jpg'
    cv2.imwrite(filename, draw)
    return filename

def drawtreshold(boxes,scores,labels,draw,file):
    # visualize detections
    for box, score, label in zip(boxes[0], scores[0], labels[0]):
        # scores are sorted so we can break    draw_box(draw, b, color=color)
        if score < 0.5:
            break

        color = label_color(label)

        b = box.astype(int)
        draw_box(draw, b, color=color, thickness=4)
    filenametres = 'static/tresholding/' + file + "-tres.jpg"
    cv2.imwrite(filenametres, draw)
    return filenametres

    filename='static/FPN/'+file+'-FPN.jpg'
    cv2.imwrite(filename, draw)
    return filename

def Predict50(filename):
    PATH_TO_TEST_IMAGES_DIR = app.config['UPLOAD_FOLDER']
    TEST_IMAGE_PATHS = [os.path.join(PATH_TO_TEST_IMAGES_DIR, filename.format(i)) for i in range(1, 2)]

    for image_path in TEST_IMAGE_PATHS:
        labels_to_names = {0: 'Normal', 1: 'Terinfeksi'}
        image = read_image_bgr(image_path)
        image2 = image

        # copy to draw on
        draw = image.copy()
        draw2 = image.copy()
        draw3 = image.copy()
        draw4 = image.copy()
        draw5 = image.copy()
        draw6 = image.copy()
        unpreprocess = filename
        # draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

        fileoriginal = app.config['UPLOAD_FOLDER'] + filename
        filename.partition('.')
        filename, separator, extension = filename.partition('.')
        fileprepocess = 'static/preprocessing/' + filename + "-preprocess.jpg"
        filenameFPN = filename + "-FPN.jpg"
        filenameFPN2 = filename + "-FPN(Unpreprocessing).jpg"

        # preprocess image for network
        preproc = preprocess_image(image)
        image = preprocess_image(image)
        image, scale = resize_image(image)

        # write Image for Preprocessing
        cv2.imwrite(fileprepocess, preproc)

        # # process image
        # start = time.time()
        # boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))
        # print("processing time: ", time.time() - start)

        boxes, scores, labels = predictdata50(image)

        # correct for image scale
        boxes /= scale

        filedrawbox = drawUntreshold(boxes, scores, labels, draw2, filenameFPN)
        # filedrawbox2 = drawUntreshold(boxes2, scores2, labels2, draw4, filenameFPN2)
        filedrawtrbox = drawtreshold(boxes, scores, labels, draw3, filenameFPN)
        # filedrawtrbox2 = drawtreshold(boxes2, scores2, labels2, draw5, filenameFPN2)

        Normal = 0
        # Normal2 = 0
        Terinfeksi = 0
        # Terinfeksi2 = 0
        # visualize detections
        for box, score, label in zip(boxes[0], scores[0], labels[0]):
            # scores are sorted so we can break    draw_box(draw, b, color=color)

            if score < 0.5:
                break

            color = label_color(label)

            b = box.astype(int)
            draw_box(draw, b, color=color, thickness=4)

            caption = "{} {:.3f}".format(labels_to_names[label], score)
            draw_caption(draw, b, caption)
            print(caption)

            if label == 0:
                Normal = Normal + 1
            else:
                Terinfeksi = Terinfeksi + 1

        Index = round(((Terinfeksi / (Normal + Terinfeksi)) * 100), 2)
        IndexVal = str(Index) + "%"
        print("Eritrosit Normal= ", Normal)
        print("Eritrosit Terinfeksi= ", Terinfeksi)
        print("Index Parasit= ", IndexVal)

        if Index > 0:
            diagnosis = "Terkena Malaria"
            print("Diagnosis Pasien = " + diagnosis)
        else:
            diagnosis = "Tidak Terkena Malaria"
            print("Diagnosis Pasien = " + diagnosis)

        # filename.partition('.')
        # filename,separator,extension=filename.partition('.')
        file = filename + '-test.jpg'
        plt.figure(figsize=(20, 20))
        plt.axis('off')
        # plt.imshow(draw)
        cv2.imwrite('static/results/' + file, draw)
        # draw=plt.gcf()
        # draw.savefig('static/results/'+file,format='jpg')
        directoryfile = "/static/results/" + file
        # plt.show()


        # return render_template('index.html')
        return directoryfile, fileprepocess,filedrawbox, filedrawtrbox,\
               Normal, Terinfeksi, IndexVal, diagnosis

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=5000)

